//
//  NYCRefreshControl.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

class NYCRefreshControl: UIRefreshControl {

    // MARK: - Initialization
    func initialize(ptrText: String) {
        tintColor = UIColor(named: "stratosphere_troposphere")
        attributedTitle = NSAttributedString(string: ptrText,
                                             attributes: [NSAttributedString.Key.foregroundColor:
                                                UIColor(named: "nickel") as Any])
    }

    // MARK: - init functions
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init() {
        super.init()
    }
}
