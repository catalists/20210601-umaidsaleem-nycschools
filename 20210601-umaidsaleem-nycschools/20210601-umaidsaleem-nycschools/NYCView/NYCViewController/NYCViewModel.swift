//
//  NYCViewModel.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

protocol NYCViewModelProtocol: AnyObject {
    func loadTableData(_ tableDataRows: [TableCellModel])
    func loadDetailData(_ detailOpenData: [GetNYCDetailData.Response])
    func showActivityIndicator()
    func hideActivityIndicator()
    func handleAPIError(apiName: String)
}

struct NYCViewModel {
    
    weak var delegate: NYCViewModelProtocol?
    
    func retrieveNYCOpenData() {
        var tableData: [TableCellModel] = []
        var detailData: [GetNYCDetailData.Response] = []
        delegate?.showActivityIndicator()
        
        let dispatchQueue = DispatchQueue(label: "nyc.opendata.queue",
                                          qos: .background, attributes: .concurrent)
        let dispatchGroup = DispatchGroup()
        
        dispatchQueue.async(group: dispatchGroup) { [self] in
            
            // GetNYCOpenData service consumption
            dispatchGroup.enter()
            GetNYCOpenDataLayer().retrieveData() { (response) in
                switch response {
                case .success(let responseModel):
                    tableData = populateNycOpenData(nycSchools: responseModel)
                    do { dispatchGroup.leave() }
                case .failure(let error):
                    Log.print(.error, error)
                    delegate?.handleAPIError(apiName: "GetNYCOpenData")
                    do { dispatchGroup.leave() }
                }
            }
            
            // GetNYCDetailData service consumption
            dispatchGroup.enter()
            GetNYCDetailDataLayer().retrieveData() { (response) in
                switch response {
                case .success(let responseModel):
                    detailData = responseModel
                    
                    do { dispatchGroup.leave() }
                case .failure(let error):
                    Log.print(.error, error)
                    delegate?.handleAPIError(apiName: "GetNYCDetailData")
                    do { dispatchGroup.leave() }
                }
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.delegate?.hideActivityIndicator()
            self.delegate?.loadTableData(tableData)
            self.delegate?.loadDetailData(detailData)
        }
    }
    
    func populateNycOpenData(nycSchools: [GetNYCOpenData.Response]) -> [TableCellModel] {
        var tableData: [TableCellModel] = []
        for index in 0..<nycSchools.count {
            let schoolObj: GetNYCOpenData.Response = nycSchools[index]
            
            // NYC Score Data
            var nycScoreData = NYCScoreObject()
            nycScoreData.dbn = schoolObj.dbn
            nycScoreData.requirement1_1 = schoolObj.requirement1_1
            nycScoreData.requirement2_1 = schoolObj.requirement2_1
            nycScoreData.requirement3_1 = schoolObj.requirement3_1
            nycScoreData.requirement4_1 = schoolObj.requirement4_1
            nycScoreData.requirement5_1 = schoolObj.requirement5_1
            nycScoreData.academicopportunities1 = schoolObj.academicopportunities1
            nycScoreData.academicopportunities2 = schoolObj.academicopportunities2
            
            // Create cell model object
            let model = NYCCellModel(schoolName: schoolObj.schoolName ?? "",
                                     city: schoolObj.city ?? "",
                                     location: schoolObj.location ?? "",
                                     nycScoresData: nycScoreData,
                                     isSelected: false)
            tableData.append(model)
        }
        return tableData
    }
    
    // This method can be moved to utilies as can be made generic array searching
    func findNYCScore(value searchValue: String, in detailData: [GetNYCDetailData.Response]) -> GetNYCDetailData.Response? {
        for detail in detailData {
            if detail.dbn == searchValue { return detail }
        }
        return nil
    }
}
