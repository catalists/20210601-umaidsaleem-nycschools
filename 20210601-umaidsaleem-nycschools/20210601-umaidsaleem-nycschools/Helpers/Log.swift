//
//  Log.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

public class Log {

    enum PrintType {
        case verbose
        case debug
        case info
        case warning
        case error
    }

    /// This function prints to the console only if the scheme is DEBUG
    static func print<T>(_ type: PrintType, _ obj: T, functionName: String = #function,
                         fileName: String = #file, lineNumber: Int = #line) {
        #if DEBUG
        let prefix = Log.getPrefixForType(type)
        let details = Log.createPrintDetails(functionName: functionName, fileName: fileName, lineNumber: lineNumber)
        Swift.print(prefix + " " + details + "\n" + "\(obj)" + "\n")
        #endif
    }

    /// This function creates the print details with function name, filename and line number
    static func createPrintDetails(functionName: String, fileName: String, lineNumber: Int) -> String {
        let filenameStr = (fileName.components(separatedBy: "/").last ?? "") as String
        return "\(filenameStr) - line: \(lineNumber)\n\(functionName)"
    }

    /// This function returns the prefix for the print type and a color-coded heart emoji :)
    static func getPrefixForType(_ type: PrintType) -> String {
        var returnString = ""
        switch type {
        case .verbose:
            returnString = "💜 VERBOSE"
        case .debug:
            returnString = "💚 DEBUG"
        case .info:
            returnString = "💙 INFO"
        case .warning:
            returnString = "💛 WARNING"
        case .error:
            returnString = "❤️ ERROR"
        }
        return returnString
    }

}
