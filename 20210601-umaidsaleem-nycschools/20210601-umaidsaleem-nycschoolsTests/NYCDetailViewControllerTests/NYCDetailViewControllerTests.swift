//
//  NYCDetailViewControllerTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class NYCDetailViewControllerTests: XCTestCase {

    private var sut: NYCDetailViewController!
    var nycScoreData = NYCScoreObject()
    
    override func setUp() {
        super.setUp()
        
        nycScoreData.dbn = "01M01"
        nycScoreData.requirement1_1 = "Requirement 1"
        nycScoreData.requirement2_1 = "Requirement 2"
        nycScoreData.requirement3_1 = "Requirement 3"
        nycScoreData.requirement4_1 = "Requirement 4"
        nycScoreData.requirement5_1 = "Requirement 5"
        nycScoreData.academicopportunities1 = "Academic Ops 1"
        nycScoreData.academicopportunities2 = "Academic Ops 2"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    private func initializeViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "NYCDetailViewController") as? NYCDetailViewController
        sut.loadViewIfNeeded()
    }
    
    func testOutletsAreConnected() {
        initializeViewController()
          
        XCTAssertNotNil(sut.req1Lbl)
        XCTAssertNotNil(sut.req2Lbl)
        XCTAssertNotNil(sut.req3Lbl)
        XCTAssertNotNil(sut.req4Lbl)
        XCTAssertNotNil(sut.req5Lbl)
        XCTAssertNotNil(sut.acadOp1Lbl)
        XCTAssertNotNil(sut.acadOp2Lbl)
        XCTAssertNotNil(sut.mathScoreLbl)
        XCTAssertNotNil(sut.readingScoreLbl)
        XCTAssertNotNil(sut.writingScoreLbl)
    }
}
