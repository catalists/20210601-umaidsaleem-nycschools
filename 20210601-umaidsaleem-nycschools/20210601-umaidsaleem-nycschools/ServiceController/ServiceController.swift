//
//  ServiceController.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

class ServiceController {

    var testingParams: [String] = []

    func encodeRequestBody<T: Encodable>(requestObject: T, name: String) -> Data? {
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(requestObject)
            let jsonString = String(data: jsonData, encoding: .utf8)
            Log.print(.verbose, "\(name) Service Request \(jsonString ?? "")")
            return jsonData
        } catch {
            Log.print(.error, "error encoding request")
            return nil
        }
    }

}
