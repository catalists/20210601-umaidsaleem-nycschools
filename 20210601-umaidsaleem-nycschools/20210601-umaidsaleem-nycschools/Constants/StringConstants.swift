//
//  StringConstants.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

public enum StringConstants {

    // MARK: - View Controller Constants
    public enum NYCView {
        static let pullToRefreshText = "Updating NYC Schools..."
        static let nycView = "NYC Schools"
        static let nycDetailView = "NYC School Details"
    }
    
    // MARK: - Error Constants
    public enum ErrorMessage {
        static let offline = "You appear to be offline. Please connect to the internet and try again."
        static let invalidURL = "Invalid URL"
        static let noData = "No Data Received"
        static let jsonDecodingError = "JSON Decoding Error"
        static let apiError = "Unable to load data from service"
        static let serviceCallFailure = "Request Failed"
        static let errorMessage = "errorMsg"
    }
    
    // MARK: - URL Constants
    public enum URLRequest {
        static let nycOpenData = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let nycDetailData = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    public enum StaticRequestParams {
        static let timeout = 30
    }
}
