//
//  NYCViewControllerTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class NYCViewControllerTests: XCTestCase {

    private var sut: NYCViewController!
    var nycScoreData = NYCScoreObject()
    
    override func setUp() {
        super.setUp()
        
        nycScoreData.dbn = "01M01"
        nycScoreData.requirement1_1 = "Requirement 1"
        nycScoreData.requirement2_1 = "Requirement 2"
        nycScoreData.requirement3_1 = "Requirement 3"
        nycScoreData.requirement4_1 = "Requirement 4"
        nycScoreData.requirement5_1 = "Requirement 5"
        nycScoreData.academicopportunities1 = "Academic Ops 1"
        nycScoreData.academicopportunities2 = "Academic Ops 2"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    private func initializeViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "NYCViewController") as? NYCViewController
        sut.loadViewIfNeeded()
    }
    
    func testOutletsAreConnected() {
        initializeViewController()
        
        // test view controller outlets are connected
        XCTAssertNotNil(sut.nycTblView)
  
        // test cell outlets are connected
        let cellModel = NYCCellModel(schoolName: "NYC High School", city: "Manhattan", location: "NY", nycScoresData: nycScoreData, isSelected: false)
        let cell = cellModel.cellInstance(sut.nycTblView, indexPath: IndexPath(row: 0, section: 0), delegate: self) as! NYCCell
        XCTAssertNotNil(cell, "cellNotNil")
        XCTAssertNotNil(cell.schoolNameLbl.text, "NYC High School")
        XCTAssertEqual(cell.schoolCityLbl.text, "Manhattan")
        XCTAssertEqual(cell.schoolLocationLbl.text, "NY")
        XCTAssertNotNil(cell.schoolNameLbl)
        XCTAssertNotNil(cell.schoolCityLbl)
        XCTAssertNotNil(cell.schoolLocationLbl)
        
    }

}
