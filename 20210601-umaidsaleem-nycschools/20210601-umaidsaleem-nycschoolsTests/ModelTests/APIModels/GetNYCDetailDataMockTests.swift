//
//  GetNYCDetailDataMockTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class GetNYCDetailDataMockTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
       
    }
        
    func testCreateResponseBody() {
        let response = GetNYCDetailData.Response(dbn: "01M292", schoolName: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", num_of_sat_test_takers: "29", sat_critical_reading_avg_score: "355", sat_math_avg_score: "404", sat_writing_avg_score: "363")

        XCTAssertEqual(response.dbn, "01M292")
        XCTAssertEqual(response.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(response.num_of_sat_test_takers, "29")
        XCTAssertEqual(response.sat_critical_reading_avg_score, "355")
        XCTAssertEqual(response.sat_math_avg_score, "404")
        XCTAssertEqual(response.sat_writing_avg_score, "363")
       
    }

}
