//
//  GetNYCDetailData.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

struct GetNYCDetailData {
    
    // MARK: - Response Struct
    struct Response: Codable {
        let dbn, schoolName, num_of_sat_test_takers: String?
        let sat_critical_reading_avg_score, sat_math_avg_score, sat_writing_avg_score: String?
    
        
        enum CodingKeys: String, CodingKey {
            // We can modify further variables based on camel casing, have done one example for demostration
            case schoolName = "school_name"
            case dbn, num_of_sat_test_takers, sat_critical_reading_avg_score
            case sat_math_avg_score, sat_writing_avg_score
        }
    }
}
