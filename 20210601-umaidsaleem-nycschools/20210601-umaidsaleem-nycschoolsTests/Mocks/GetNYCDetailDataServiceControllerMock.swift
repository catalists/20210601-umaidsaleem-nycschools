//
//  GetNYCDetailDataServiceControllerMock.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/2/21.
//

@testable import _0210601_umaidsaleem_nycschools

class GetNYCDetailDataServiceControllerMock: ServiceController, GetNYCDetailDataServiceControllerProtocol {
    func getNYCDetailData(_ completion: @escaping (ServiceOutcome<[GetNYCDetailData.Response]>) -> Void) {
        if testingParams.first! == "testSuccess" {
            let response = GetNYCDetailData.Response(dbn: "01M292", schoolName: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", num_of_sat_test_takers: "29", sat_critical_reading_avg_score: "355", sat_math_avg_score: "404", sat_writing_avg_score: "363")
            completion(.success([response]))
        } else if testingParams.first! == "testFailure" {
            let errorObj = ErrorResponseModel(code: nil, message: "errorMsg")
            completion(.failure(errorObj))
        }
    }
}
