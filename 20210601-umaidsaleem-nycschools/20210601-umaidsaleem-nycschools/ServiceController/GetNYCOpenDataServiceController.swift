//
//  GetNYCOpenDataServiceController.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

protocol GetNYCOpenDataServiceControllerProtocol {
    func getNYCOpenData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCOpenData.Response]>) -> Void)
}

class GetNYCOpenDataServiceController: ServiceController, GetNYCOpenDataServiceControllerProtocol {
    
    func getNYCOpenData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCOpenData.Response]>) -> Void) {
        
        guard let url = URL(string: StringConstants.URLRequest.nycOpenData) else {
            let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.invalidURL)
            completion(.failure(errorObj))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            // fail completion for Error
            guard let objData = data else {
                let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.noData)
                completion(.failure(errorObj))
                return
            }
            
            // Validate for blank data and URL response status code
            if let objURLResponse = response as? HTTPURLResponse {
                // We have data validate for JSON and convert in JSON
                do {
                    let getNYCResponse = try JSONDecoder().decode([GetNYCOpenData.Response].self, from: objData)
                    if objURLResponse.statusCode == 200 {
                        completion(.success(getNYCResponse))
                    }
                } catch {
                    let errorObj = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.jsonDecodingError)
                    completion(.failure(errorObj))
                }
            }
        }
        task.resume()
    }
}
