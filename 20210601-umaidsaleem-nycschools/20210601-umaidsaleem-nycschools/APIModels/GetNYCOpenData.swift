//
//  GetNYCOpenData.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

struct GetNYCOpenData {
    
    // MARK: - Response Struct
    struct Response: Codable {
        let dbn, schoolName, boro, overview_paragraph, school_10th_seats : String?
        let academicopportunities1, academicopportunities2, ell_programs, neighborhood : String?
        let building_code, location, phone_number, fax_number, school_email, website, subway, bus: String?
        let grades2018, finalgrades, total_students, extracurricular_activities, school_sports, attendance_rate: String?
        let pct_stu_enough_variety, pct_stu_safe, school_accessibility_description, directions1: String?
        let requirement1_1, requirement2_1, requirement3_1, requirement4_1, requirement5_1, offer_rate1: String?
        let program1, code1, interest1, method1, seats9ge1, grade9gefilledflag1, grade9geapplicants1: String?
        let seats9swd1, grade9swdfilledflag1, grade9swdapplicants1, seats101, admissionspriority11: String?
        let admissionspriority21, admissionspriority31, grade9geapplicantsperseat1, grade9swdapplicantsperseat1: String?
        let primary_address_line_1, city, zip, state_code, latitude, longitude, community_board, council_district: String?
        let census_tract, bin, bbl, nta, borough: String?
        
        
        enum CodingKeys: String, CodingKey {
            // We can modify further variables based on camel casing, have done one example for demostration
            case schoolName = "school_name"
            case dbn, boro, overview_paragraph, school_10th_seats
            case academicopportunities1, academicopportunities2, ell_programs, neighborhood
            case building_code, location, phone_number, fax_number, school_email, website, subway, bus
            case grades2018, finalgrades, total_students, extracurricular_activities, school_sports, attendance_rate
            case pct_stu_enough_variety, pct_stu_safe, school_accessibility_description, directions1
            case requirement1_1, requirement2_1, requirement3_1, requirement4_1, requirement5_1, offer_rate1
            case program1, code1, interest1, method1, seats9ge1, grade9gefilledflag1, grade9geapplicants1
            case seats9swd1, grade9swdfilledflag1, grade9swdapplicants1, seats101, admissionspriority11
            case admissionspriority21, admissionspriority31, grade9geapplicantsperseat1, grade9swdapplicantsperseat1
            case primary_address_line_1, city, zip, state_code, latitude, longitude, community_board, council_district
            case census_tract, bin, bbl, nta, borough
        }
        
    }
}
