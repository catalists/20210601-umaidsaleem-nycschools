//
//  GetNYCOpenDataServiceControllerTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class GetNYCOpenDataServiceControllerTests: XCTestCase {

    private var sut: GetNYCOpenDataServiceController?

    override func setUp() {
        super.setUp()
        sut = GetNYCOpenDataServiceController()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testSuccess() {
        NetworkManagerMock.testingParams = ["GetNYCOpenDataSuccess"]
        sut?.getNYCOpenData({ (response) in
            switch response {
            case .success(_):
                break
            case .failure(_):
                XCTFail()
            }
        })
    }
    
    func testFailure() {
        NetworkManagerMock.testingParams = ["TestFailure"]
        sut?.getNYCOpenData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(_):
                break
            }
        })
    }
    
    func testNilData() {
        NetworkManagerMock.testingParams = ["TestNilData"]
        sut?.getNYCOpenData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.noData)
            }
        })
    }

    func testJSONDecodingError() {
        NetworkManagerMock.testingParams = ["JSONDecodingError"]
        sut?.getNYCOpenData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.jsonDecodingError)
            }
        })
    }
}
