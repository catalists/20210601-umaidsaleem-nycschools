//
//  GetNYCDetailDataLayer.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

protocol GetNYCDetailDataLayerProtocol {
    func retrieveData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCDetailData.Response]>) -> Void)
}

class GetNYCDetailDataLayer: DataLayer, GetNYCDetailDataLayerProtocol {
    
    var callback: ((ServiceOutcome<[GetNYCDetailData.Response]>) -> Void)?
    
    func retrieveData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCDetailData.Response]>) -> Void) {
        callback = completion
        // if no internet connection, go diretly to the local storage
        guard Reachability().networkConnected() else {
            let err = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.offline)
            callback!(.failure(err))
            // We can load offline data from database in case of failures
            return
        }
        
        // call service controller
        GetNYCDetailDataServiceController().getNYCDetailData() { (response) in
            switch response {
            case .success(let model):
                // Callback with updated object
                let responseModel: [GetNYCDetailData.Response] = model
                self.callback!(.success(responseModel))
            case .failure(let error):
                // We can load offline data from database in case of failures
                Log.print(.error, error)
            }
        }
    }
}
