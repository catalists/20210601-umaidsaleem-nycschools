//
//  NYCViewController.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

class NYCViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var nycTblView: UITableView!
    
    // MARK: - Properties
    var viewModel = NYCViewModel()
    var tableData = [TableCellModel]()
    var detailData = [GetNYCDetailData.Response]()
    let activityIndicator = NYCLoadingView()
    let refreshControl = NYCRefreshControl()
    
    // MARK: - App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setup()
        setAccessibilityIdentifiers()
    }
    
    // MARK: - Setup UI
    private func setup() {
        title = StringConstants.NYCView.nycView
        view.backgroundColor = UIColor(named: "white_charcoal")
        // Show the navigation bar with large title and make it small while scrolling
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        setupTableView()
    }
    
    private func setupViewModel() {
        viewModel.delegate = self
        if tableData.isEmpty {
            viewModel.retrieveNYCOpenData()
        }
    }
    
    // MARK: - Setup Accessibility
    private func setAccessibilityIdentifiers() {
        view.accessibilityIdentifier = "NYCViewController"
        nycTblView.accessibilityIdentifier = "nycTblView"
    }
    
    @objc private func refreshNYCData() {
        guard Reachability().networkConnected() else {
            showToast(message: StringConstants.ErrorMessage.offline, font: .systemFont(ofSize: 14.0))
            controlEndRefreshing()
            return
        }
        viewModel.retrieveNYCOpenData()
    }
    
    
    private func controlEndRefreshing() {
        CATransaction.begin()
        CATransaction.setCompletionBlock { () -> Void in
            /* wait for endRefreshing animation to complete
             before reloadData so table view does not flicker to top
             then continue endRefreshing animation */
            self.nycTblView.reloadData()
            self.nycTblView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
        
        refreshControl.endRefreshing()
        CATransaction.commit()
    }
    
    private func reloadVisibleCells() {
        guard let visibleCells = nycTblView.indexPathsForVisibleRows else { return }
        nycTblView.reloadRows(at: visibleCells, with: .automatic)
    }
}

// MARK: - NYCViewModel Protocols
extension NYCViewController: NYCViewModelProtocol {
    
    func loadTableData(_ tableDataRows: [TableCellModel]) {
        guard !tableDataRows.isEmpty else { return }
        DispatchQueue.main.async {
            self.tableData = tableDataRows
            self.controlEndRefreshing()
        }
    }
    
    func loadDetailData(_ detailOpenData: [GetNYCDetailData.Response]) {
        guard !detailOpenData.isEmpty else { return }
        detailData = detailOpenData
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.show(on: self.view)
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.hide()
        }
    }
    
    func handleAPIError(apiName: String) {
        DispatchQueue.main.async {
            self.showToast(message: "\(StringConstants.ErrorMessage.apiError) from \(apiName)", font: .systemFont(ofSize: 14.0))
        }
    }
}

// MARK: - TableView Delegate Functions
extension NYCViewController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        nycTblView.delegate = self
        nycTblView.dataSource = self
        nycTblView.backgroundColor = UIColor.clear
        nycTblView.separatorStyle = .none
        nycTblView.estimatedRowHeight = 100
        nycTblView.rowHeight = UITableView.automaticDimension
        
        refreshControl.initialize(ptrText: StringConstants.NYCView.pullToRefreshText)
        refreshControl.addTarget(self, action: #selector(refreshNYCData), for: .valueChanged)
        nycTblView.refreshControl = refreshControl
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableData[indexPath.row].cellInstance(tableView, indexPath: indexPath, delegate: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableRowSelected(indexPath: indexPath)
    }
    
    private func tableRowSelected(indexPath: IndexPath) {
        guard var model = tableData[indexPath.row] as? NYCCellModel,
              let cell = nycTblView.cellForRow(at: indexPath) as? NYCCell else { return }
        
        // Set model selected and reload visible cells
        model.isSelected = true
        tableData.remove(at: indexPath.row)
        tableData.insert(model, at: indexPath.row)
        cell.setCellSelected(true)
        reloadVisibleCells()
        
        // Fetch value of NYC detail from details array
        var nycDetail: GetNYCDetailData.Response? = nil
        if detailData.count > 0 {
            nycDetail = viewModel.findNYCScore(value: model.nycScoresData?.dbn ?? "", in: detailData) ?? nil
        }
        
        // Call NYC Detail view controller to show SAT scores
        guard let nycDetailViewController = NYCDetailViewController.storyboard.instantiateViewController(
                withIdentifier: "NYCDetailViewController") as? NYCDetailViewController else { return }
        nycDetailViewController.nycScoresData = model.nycScoresData
        nycDetailViewController.nycDetail = nycDetail
        navigationController?.pushViewController(nycDetailViewController, animated: true)
    }
}

// MARK: - Additional Helper class
extension NYCViewController {
    
    // This can be reusable method from UI Components
    func showToast(message : String, font: UIFont) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

// MARK: - NYCCellDelegate
extension NYCViewController: NYCCellDelegate {    
    func arrowSelected(indexPath: IndexPath) {
        tableRowSelected(indexPath: indexPath)
    }
}
