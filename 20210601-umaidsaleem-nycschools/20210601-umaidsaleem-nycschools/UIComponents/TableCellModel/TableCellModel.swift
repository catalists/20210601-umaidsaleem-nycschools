//
//  TableCellModel.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

protocol TableCellModel {
    func cellInstance(_ tableView: UITableView,
                      indexPath: IndexPath,
                      delegate: AnyObject?) -> UITableViewCell
}
