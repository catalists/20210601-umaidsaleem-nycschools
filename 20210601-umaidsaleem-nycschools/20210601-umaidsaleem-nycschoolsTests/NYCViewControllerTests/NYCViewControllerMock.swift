//
//  NYCViewControllerMock.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class NYCViewControllerMock: UIViewController, NYCViewModelProtocol {
  
    // MARK: - Properties
    var viewModel = NYCViewModel()
    var tableData = [TableCellModel]()
    var detailData = [GetNYCDetailData.Response]()
    var showActivityIndicatorCalled = 0
    var hideActivityIndicatorCalled = 0
    var handleNYCCalled = 0
    
    // MARK: - Expectations
    var handleNYCOpenDataErrorExpectation: XCTestExpectation?
    var loadTableDataExpectation: XCTestExpectation?
    var loadDetailDataExpectation: XCTestExpectation?
    var showIndicatorExpectation: XCTestExpectation?
    var hideIndicatorExpectation: XCTestExpectation?
    
    // MARK: - NYCViewModel Methods
    func loadTableData(_ tableDataRows: [TableCellModel]) {
        tableData = tableDataRows
        loadTableDataExpectation?.fulfill()
    }
    
    func loadDetailData(_ detailOpenData: [GetNYCDetailData.Response]) {
        detailData = detailOpenData
        loadDetailDataExpectation?.fulfill()
    }
    
    func handleAPIError(apiName: String) {
        handleNYCCalled += 1
        handleNYCOpenDataErrorExpectation?.fulfill()
    }
        
    func showActivityIndicator() {
        showActivityIndicatorCalled += 1
        showIndicatorExpectation?.fulfill()
    }
    
    func hideActivityIndicator() {
        hideActivityIndicatorCalled += 1
        hideIndicatorExpectation?.fulfill()
    }
}

