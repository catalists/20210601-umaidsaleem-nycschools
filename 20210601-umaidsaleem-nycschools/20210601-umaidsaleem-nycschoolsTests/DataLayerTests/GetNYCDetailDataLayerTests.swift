//
//  GetNYCDetailDataLayerTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/2/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class GetNYCDetailDataLayerTests: XCTestCase {

    private var sut: GetNYCDetailDataLayer?
    private var mockServiceController = GetNYCDetailDataServiceControllerMock()

    override func setUp() {
        super.setUp()
        sut = GetNYCDetailDataLayer()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSuccessfulResponse() {
        mockServiceController.testingParams = ["testSuccess"]
        sut?.retrieveData({(response) in
            switch response {
            case .success(_): break
                // success block is a passing test
            case .failure(_):
                XCTFail()
            }
        })
    }

    func testFailureResponse() {
        mockServiceController.testingParams = ["testFailure"]
        sut?.retrieveData({(response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.errorMessage)
            }
        })
    }

}
