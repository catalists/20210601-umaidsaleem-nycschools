//
//  GetNYCDetailDataServiceControllerTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class GetNYCDetailDataServiceControllerTests: XCTestCase {

    private var sut: GetNYCDetailDataServiceController?
    
    override func setUp() {
        super.setUp()
        sut = GetNYCDetailDataServiceController()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testSuccess() {
        NetworkManagerMock.testingParams = ["GetNYCDetailDataSuccess"]
        sut?.getNYCDetailData({ (response) in
            switch response {
            case .success(_):
                break
            case .failure(_):
                XCTFail()
            }
        })
    }
    
    func testFailure() {
        NetworkManagerMock.testingParams = ["TestFailure"]
        sut?.getNYCDetailData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(_):
                break
            }
        })
    }
    
    func testNilData() {
        NetworkManagerMock.testingParams = ["TestNilData"]
        sut?.getNYCDetailData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.noData)
            }
        })
    }

    func testJSONDecodingError() {
        NetworkManagerMock.testingParams = ["JSONDecodingError"]
        sut?.getNYCDetailData({ (response) in
            switch response {
            case .success(_):
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(error.code, nil)
                XCTAssertEqual(error.message, StringConstants.ErrorMessage.jsonDecodingError)
            }
        })
    }

}
