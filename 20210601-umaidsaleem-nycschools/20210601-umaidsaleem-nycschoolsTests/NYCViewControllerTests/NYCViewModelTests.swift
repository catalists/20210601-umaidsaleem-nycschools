//
//  NYCViewModelTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class NYCViewModelTests: XCTestCase {
    
    private var sut: NYCViewModel!
    private var mockView = NYCViewControllerMock()
    
    override func setUp() {
        super.setUp()
        sut = NYCViewModel()
        sut.delegate = mockView
        mockView.viewModel = sut
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCreateTableDataEmpty() {
        let successExpectation = XCTestExpectation(description: "successExpectation")
        mockView.loadTableDataExpectation = successExpectation
        let tableData: [TableCellModel] = mockView.viewModel.populateNycOpenData(nycSchools: [])
        // wait(for: [successExpectation], timeout: 1)  // TODO : need to fix once finalize
        // Validate results
        XCTAssertEqual(mockView.tableData.count, tableData.count)
    }
    
    func testCreateTableDataPopulated() {
        let successExpectation = XCTestExpectation(description: "successExpectation")
        mockView.loadTableDataExpectation = successExpectation
        let nycData: [GetNYCOpenData.Response] = [
            GetNYCOpenData.Response(dbn: "0012", schoolName: "NYC School", boro: "", overview_paragraph: "", school_10th_seats: "", academicopportunities1: "", academicopportunities2: "", ell_programs: "", neighborhood: "", building_code: "", location: "", phone_number: "", fax_number: "", school_email: "", website: "", subway: "", bus: "", grades2018: "", finalgrades: "", total_students: "", extracurricular_activities: "", school_sports: "", attendance_rate: "", pct_stu_enough_variety: "", pct_stu_safe: "", school_accessibility_description: "", directions1: "", requirement1_1: "", requirement2_1: "", requirement3_1: "", requirement4_1: "", requirement5_1: "", offer_rate1: "", program1: "", code1: "", interest1: "", method1: "", seats9ge1: "", grade9gefilledflag1: "", grade9geapplicants1: "", seats9swd1: "", grade9swdfilledflag1: "", grade9swdapplicants1: "", seats101: "", admissionspriority11: "", admissionspriority21: "", admissionspriority31: "", grade9geapplicantsperseat1: "", grade9swdapplicantsperseat1: "", primary_address_line_1: "", city: "", zip: "", state_code: "", latitude: "", longitude: "", community_board: "", council_district: "", census_tract: "", bin: "", bbl: "", nta: "", borough: ""),
            GetNYCOpenData.Response(dbn: "00123", schoolName: "NYC School 123", boro: "", overview_paragraph: "", school_10th_seats: "", academicopportunities1: "", academicopportunities2: "", ell_programs: "", neighborhood: "", building_code: "", location: "", phone_number: "", fax_number: "", school_email: "", website: "", subway: "", bus: "", grades2018: "", finalgrades: "", total_students: "", extracurricular_activities: "", school_sports: "", attendance_rate: "", pct_stu_enough_variety: "", pct_stu_safe: "", school_accessibility_description: "", directions1: "", requirement1_1: "", requirement2_1: "", requirement3_1: "", requirement4_1: "", requirement5_1: "", offer_rate1: "", program1: "", code1: "", interest1: "", method1: "", seats9ge1: "", grade9gefilledflag1: "", grade9geapplicants1: "", seats9swd1: "", grade9swdfilledflag1: "", grade9swdapplicants1: "", seats101: "", admissionspriority11: "", admissionspriority21: "", admissionspriority31: "", grade9geapplicantsperseat1: "", grade9swdapplicantsperseat1: "", primary_address_line_1: "", city: "", zip: "", state_code: "", latitude: "", longitude: "", community_board: "", council_district: "", census_tract: "", bin: "", bbl: "", nta: "", borough: "")]
        
        mockView.viewModel.retrieveNYCOpenData()
        let tableData: [TableCellModel] = mockView.viewModel.populateNycOpenData(nycSchools: nycData)
        mockView.tableData = tableData
        // wait(for: [successExpectation], timeout: 1)     // TODO : need to fix once finalize
        // Validate results
        XCTAssertEqual(mockView.tableData.count, 2)
        
    }
    
}
