//
//  HTTPResponse.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

public struct HTTPResponse {
    var data: Data?
    var httpURLResponse: HTTPURLResponse?
    var responseDate: Date?
}
