//
//  ErrorResponseModelTests.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class ErrorResponseModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testErrorResponseInitialization() {
        let errorResponse = ErrorResponseModel(code: "testCode", message: "testMessage")
        XCTAssertEqual(errorResponse.code, "testCode")
        XCTAssertEqual(errorResponse.message, "testMessage")

        let emptyResponse = ErrorResponseModel(code: nil, message: nil)
        XCTAssertEqual(emptyResponse.code, nil)
        XCTAssertEqual(emptyResponse.message, nil)
    }

    func testInitializationData() {
        let data = MockHelper().readJsonFromFile(filename: "ErrorResponseMock")
        let errorResponse = ErrorResponseModel(data: data!)
        XCTAssertEqual(errorResponse.code, "200")
        XCTAssertEqual(errorResponse.message, "Test Error Message")

        let errorResponseFail = ErrorResponseModel(data: Data())
        XCTAssertEqual(errorResponseFail.code, nil)
        XCTAssertEqual(errorResponseFail.message, nil)
    }
}
