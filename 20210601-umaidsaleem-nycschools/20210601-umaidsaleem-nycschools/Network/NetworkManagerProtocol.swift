//
//  NetworkManagerProtocol.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

protocol NetworkManagerProtocol {
    func request(_ completion: @escaping (_ response: ServiceOutcome<HTTPResponse>) -> Void)
    var request: URLRequest! { get set }
    var pwdReset: Bool { get set }
}
