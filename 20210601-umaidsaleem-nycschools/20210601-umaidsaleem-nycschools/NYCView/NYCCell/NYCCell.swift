//
//  NYCCell.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

protocol NYCCellDelegate: AnyObject {
    func arrowSelected(indexPath: IndexPath)
}

struct NYCScoreObject {
    var dbn: String?
    var requirement1_1: String?
    var requirement2_1: String?
    var requirement3_1: String?
    var requirement4_1: String?
    var requirement5_1: String?
    var academicopportunities1: String?
    var academicopportunities2: String?
}


struct NYCCellModel: TableCellModel {
    
    // MARK: - Properties
    var schoolName: String?
    var city: String?
    var location: String?
    var isSelected = false
    var nycScoresData: NYCScoreObject?
    
    init(schoolName: String, city: String, location: String, nycScoresData: NYCScoreObject,  isSelected: Bool) {
        self.schoolName = schoolName
        self.city = city
        self.location = location
        self.isSelected = isSelected
        self.nycScoresData = nycScoresData
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath, delegate: AnyObject?) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NYCCell", for: indexPath)
                as? NYCCell else { return UITableViewCell() }
        cell.delegate = delegate as? NYCCellDelegate
        cell.configure(viewModel: self, indexPath: indexPath)
        return cell
    }
}

class NYCCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var schoolNameLbl: UILabel!
    @IBOutlet weak var schoolLocationLbl: UILabel!
    @IBOutlet weak var schoolCityLbl: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    
    // MARK: - Properties
    weak var delegate: NYCCellDelegate?
    var viewModel: NYCCellModel?
    var nycScoresData: NYCScoreObject?
    var indexPath = IndexPath(row: 0, section: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    // MARK: - Setup methods
    private func setup() {
        backgroundColor = UIColor(named: "white_charcoal")
        schoolNameLbl.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        schoolLocationLbl.font = UIFont.systemFont(ofSize: 14)
        schoolCityLbl.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        arrowButton.tintColor = UIColor(named: "nickel")
        arrowButton.setImage(UIImage(named: "rightArrow"), for: .normal)
    }
    
    func configure(viewModel: NYCCellModel, indexPath: IndexPath) {
        self.viewModel = viewModel
        self.indexPath = indexPath
        schoolNameLbl.text = viewModel.schoolName
        schoolLocationLbl.text = viewModel.location
        schoolCityLbl.text = viewModel.city
        nycScoresData = viewModel.nycScoresData
        
        setAccessibilityIdentifiers(index: indexPath.row)
        setCellSelected(viewModel.isSelected)
    }
    
    func setAccessibilityIdentifiers(index: Int) {
        schoolNameLbl.accessibilityIdentifier = "schoolNameLbl\(index)"
        schoolLocationLbl.accessibilityIdentifier = "schoolLocationLbl\(index)"
        schoolCityLbl.accessibilityIdentifier = "schoolCityLbl\(index)"
        arrowButton.accessibilityIdentifier = "arrowButton\(index)"
    }
    
    func setCellSelected(_ selected: Bool) {
        if selected {
            arrowButton.tintColor = UIColor(named: "stratosphere_troposphere")
            backgroundColor = UIColor(named: "cirrus_charcoal")
        } else {
            arrowButton.tintColor = UIColor(named: "nickel")
            backgroundColor = UIColor(named: "white_carbon")
        }
        layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
