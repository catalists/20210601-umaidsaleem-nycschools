//
//  NYCLoadingView.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

class NYCLoadingView: UIView {

    // MARK: - Outlets
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!

    // MARK: - Properties
    var titleText: String?

    // MARK: - init functions
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    private func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "NYCLoadingView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        view?.frame = bounds
        view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if let myView = view {
            addSubview(myView)
        }
    }

    // MARK: - Private Setup Functions
    private func setup() {
        loadViewFromNib()
        setupSubviews()
    }

    private func addConstraints() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
        leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 0).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: 0).isActive = true
        layoutIfNeeded()
    }

    private func setupSubviews() {
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)

        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 5.0
        containerView.backgroundColor = UIColor(named: "containerColor")

        activityIndicator.style = UIActivityIndicatorView.Style.medium
        activityIndicator.color = UIColor(named: "activityIndicator")
        activityIndicator.startAnimating()

        titleLabel.font = UIFont.preferredFont(forTextStyle: .body)
        titleLabel.text = titleText
    }

    // MARK: - Public Functions
    func show(on myView: UIView, text: String? = "Please wait...") {
        titleText = text
        setupSubviews()
        /* Add subview must be called before addConstraints
           because addConstraints uses the superview property */
        myView.addSubview(self)
        addConstraints()
    }

    func hide() {
        activityIndicator.stopAnimating()
        removeFromSuperview()
    }
}
