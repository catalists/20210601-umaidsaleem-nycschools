//
//  GetNYCOpenDataLayer.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import Foundation

protocol GetNYCOpenDataLayerProtocol {
    func retrieveData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCOpenData.Response]>) -> Void)
}

class GetNYCOpenDataLayer: DataLayer, GetNYCOpenDataLayerProtocol {
    
    var callback: ((ServiceOutcome<[GetNYCOpenData.Response]>) -> Void)?
    
    func retrieveData(_ completion: @escaping (_ response: ServiceOutcome<[GetNYCOpenData.Response]>) -> Void) {
        callback = completion
        // if no internet connection, go diretly to the local storage
        guard Reachability().networkConnected() else {
            let err = ErrorResponseModel(code: nil, message: StringConstants.ErrorMessage.offline)
            callback!(.failure(err))
            // We can load offline data from database in case of failures
            return
        }
        
        // call service controller
        GetNYCOpenDataServiceController().getNYCOpenData() { (response) in
            switch response {
            case .success(let model):
                // Callback with updated object
                let responseModel: [GetNYCOpenData.Response] = model
                self.callback!(.success(responseModel))
            case .failure(let error):
                // We can load offline data from database in case of failures
                Log.print(.error, error)
            }
        }
    }
}
