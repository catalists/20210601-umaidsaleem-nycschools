//
//  NYCDetailViewController.swift
//  20210601-umaidsaleem-nycschools
//
//  Created by Umaid Saleem on 6/1/21.
//

import UIKit

class NYCDetailViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var req1Lbl: UILabel!
    @IBOutlet weak var req2Lbl: UILabel!
    @IBOutlet weak var req3Lbl: UILabel!
    @IBOutlet weak var req4Lbl: UILabel!
    @IBOutlet weak var req5Lbl: UILabel!
    @IBOutlet weak var acadOp1Lbl: UILabel!
    @IBOutlet weak var acadOp2Lbl: UILabel!
    @IBOutlet weak var mathScoreLbl: UILabel!
    @IBOutlet weak var readingScoreLbl: UILabel!
    @IBOutlet weak var writingScoreLbl: UILabel!
    
    // MARK: - Properties
    static var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var nycScoresData: NYCScoreObject?
    var nycDetail: GetNYCDetailData.Response?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setAccessibilityIdentifiers()
    }
    
    // MARK: - Setup UI
    private func setup() {
        title = StringConstants.NYCView.nycDetailView
        view.backgroundColor = UIColor(named: "white_charcoal")
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // Set Requirement labels
        let notAvailable = "N/A"
        if let req1 =  nycScoresData?.requirement1_1 { req1Lbl.text = req1 } else { req1Lbl.text = notAvailable }
        if let req2 =  nycScoresData?.requirement2_1 { req2Lbl.text = req2 } else { req2Lbl.text = notAvailable }
        if let req3 =  nycScoresData?.requirement3_1 { req3Lbl.text = req3 } else { req3Lbl.text = notAvailable }
        if let req4 =  nycScoresData?.requirement4_1 { req4Lbl.text = req4 } else { req4Lbl.text = notAvailable }
        if let req5 =  nycScoresData?.requirement5_1 { req5Lbl.text = req5 } else { req5Lbl.text = notAvailable }
        
        // Set Academic opportunities labels
        if let acad1 = nycScoresData?.academicopportunities1 {  acadOp1Lbl.text = acad1 } else { acadOp1Lbl.text = notAvailable }
        if let acad2 = nycScoresData?.academicopportunities2 {  acadOp2Lbl.text = acad2 } else { acadOp2Lbl.text = notAvailable }
        
        // Set SAT Score labels
        if let mathScore = nycDetail?.sat_math_avg_score { mathScoreLbl.text = mathScore } else { mathScoreLbl.text = notAvailable }
        if let readingScore = nycDetail?.sat_critical_reading_avg_score { readingScoreLbl.text = readingScore } else { readingScoreLbl.text = notAvailable }
        if let writingScore = nycDetail?.sat_writing_avg_score { writingScoreLbl.text = writingScore } else { writingScoreLbl.text = notAvailable }
    }
    
    // MARK: - Setup Accessibility
    private func setAccessibilityIdentifiers() {
        view.accessibilityIdentifier = "NYCDetailViewController"
        req1Lbl.accessibilityIdentifier = "requirementLbl1"
        req2Lbl.accessibilityIdentifier = "requirementLbl2"
        req3Lbl.accessibilityIdentifier = "requirementLbl3"
        req4Lbl.accessibilityIdentifier = "requirementLbl4"
        req5Lbl.accessibilityIdentifier = "requirementLbl5"
        acadOp1Lbl.accessibilityIdentifier = "academicOpportunitiesLbl1"
        acadOp2Lbl.accessibilityIdentifier = "academicOpportunitiesLbl2"
        mathScoreLbl.accessibilityIdentifier = "mathScoreLbl"
        readingScoreLbl.accessibilityIdentifier = "readingScoreLbl"
        writingScoreLbl.accessibilityIdentifier = "writingScoreLbl"
    }
}
