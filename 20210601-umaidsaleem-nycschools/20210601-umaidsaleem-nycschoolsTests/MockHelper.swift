//
//  MockHelper.swift
//  20210601-umaidsaleem-nycschoolsTests
//
//  Created by Umaid Saleem on 6/1/21.
//

import XCTest
@testable import _0210601_umaidsaleem_nycschools

class MockHelper {

    public init() {}
    
    func getMockGetNYCOpenDataResponse() -> GetNYCOpenData.Response? {
        guard let data = readJsonFromFile(filename: "GetNYCOpenDataMock") else { return nil }
        do {
            let getOpenDataResponse = try JSONDecoder().decode(GetNYCOpenData.Response.self, from: data)
            return getOpenDataResponse
        } catch { return nil }
    }
    
    func getMockGetNYCDetailDataResponse() -> GetNYCDetailData.Response? {
        guard let data = readJsonFromFile(filename: "GetNYCDetailDataMock") else { return nil }
        do {
            let getDetailDataResponse = try JSONDecoder().decode(GetNYCDetailData.Response.self, from: data)
            return getDetailDataResponse
        } catch { return nil }
    }

    func readJsonFromFile(filename: String) -> Data? {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: filename, withExtension: "json") else { return nil }
        do {
            let data = try Data(contentsOf: url, options: .mappedIfSafe)
            return data
        } catch { return nil }
    }
}
